// Read the process.txt file to know the installation process and code procedure

import React, {Component} from 'react';
import {Platform,
       StyleSheet, 
       View,
       TouchableOpacity,
       Text } from 'react-native';


// designing is done using native base components       
import {Container,Content, Icon} from 'native-base';

// for google signin integration
import { GoogleSignin } from 'react-native-google-signin';
import firebase from 'react-native-firebase'


// for instagram login
import InstagramLogin from 'react-native-instagram-login'

// create the main class
export default class App extends Component{
  constructor(props){
    super(props);
    this.state={ 
      token:null
    }
  }

  // view function displaying the two social network icons
  render() {
    return (
        <Container>
            <Content  contentContainerStyle={{width:'100%',height:'100%',
                      alignItems:'center',
                      justifyContent:'center'}}>

              <Text style={{fontSize:25,marginTop:'10%'}}>Social Integration</Text>
                
                  
                  <View style={{flexDirection:'row',marginTop:'5%'}}>

                      {/* button made for google singin */}
                      <View style={{width:'20%',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.onGoogleSignin()}>
                        <Icon name='google' type={'FontAwesome'} style={{fontSize:30}}/>
                        </TouchableOpacity>
                      </View>

                      {/* button for instagram login */}
                      <View style={{width:'20%',alignItems:'center'}}>
                      {/* give the instagram modal ref to get open */}
                      <TouchableOpacity onPress={() => this.refs.instagramLogin.show()}> 
                      <Icon name='instagram' type={'FontAwesome'} style={{fontSize:30}}/>
                      </TouchableOpacity>
                      </View>

                    </View>


                {/* when the instagram token is generated then we the token will be shown here  */}
                  <View style={{marginTop:'10%'}}>
                        {(this.state.token != null) &&
                        <Text style={{fontSize:20}}>Instagram Login Token {this.state.token}</Text> }
                  </View>
                  
                  {/* instagram login module  */}
                   <InstagramLogin
                        ref= {'instagramLogin'}
                        clientId='ab8cb96b30704532a93630828b0ff0f0' //pass the client id which is generated from instagram developer account
                        redirectUrl='http://www.sachtechsolution.com' // pass the redirect url we put it in the instagram valid redirect url when generating client id
                        scopes={['public_content', 'follower_list']}
                        onLoginSuccess={(code) => {alert("Success Token :- " + code)}}
                        onLoginFailure={(data) => {alert("Failure :- "+data)}}
                    />


            </Content>
        </Container>
    );
  }

   // google signin function
   async onGoogleSignin() {
    try {
      // add any configuration settings here:
      await GoogleSignin.configure();
      const data = await GoogleSignin.signIn();
      // create a new firebase credential with the token
      const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken)
      // login with credential
      const firebaseUserCredential = await firebase.auth().signInWithCredential(credential);
      

      //after the google signed in successfully an alert will show the data from it
      alert(JSON.stringify(firebaseUserCredential.user.toJSON())); 
    } catch (e) {
      // if there is some error in that then alert with error will show
      alert(e)
    }
  }

}

// styling the components
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }
});
