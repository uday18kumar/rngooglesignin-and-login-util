
// Just pass the filename or the value in the corresponding function and get it is valid or not

// Validate email
export const validateEmail = (string="") => {
    //pattern to check whether email is in correct form or not
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let result =  re.test(String(string).toLowerCase());
    return result;  
}

// Validate username
export const validateUsername = (string="") => {
    // to check username is not smaller than 3 
    if(string.length < 3){
        return false
    }
    return true;  
}

// Validate password
export const validatePassword = (string="") => {
    // password length must be greater than 6 or not
if(string.length< 6){
    return false;
  }
  return true;
}

// Validate phone number
export const validatePhoneNum = (string="") => {
    let allNumberic = /^[0-9]+$/; // to check whether the string is numberic or not
    let phoneno = /^\d{10}$/; // to check whether it has 10 char or not
    if(string.match(allNumberic)){
        if((string.match(phoneno)))
            return true;  
        else
            return false;
    }
    else
      return false;
}

// Validate Image
export const validateImageExtension = (string="") => {
    // to check that file is an image or not by its extension
    let Extension = string.substring(string.lastIndexOf('.') + 1).toLowerCase();
    
    if (Extension == "gif" || Extension == "png" || Extension == "bmp"
    || Extension == "jpeg" || Extension == "jpg"){
        return true
    }
    else{
        return false
    }
}


// validate address
export const validateAddress = (string='') => {
    // to check the address is in alphanumberic form and not any special character is written
    let  letters = /^[0-9a-zA-Z]+$/;
    if(string.match(letters))
    {
    return true;
    }
    else
    {
    return false;
    }
}

// validate zip code or pin code
export const validateZipcode = (string='') => {
    // to check zip code is all numberic or not
    var numbers = /^[0-9]+$/;
        if(string.match(numbers))
            return true;
        else
            return false;
}






